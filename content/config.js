var config = {
	"domain": "services.oag-fundacion.org",
	"protocol": "https",
	"resourcesPath": "resources",
	"title": "Bienvenido a services.oag-fundacion.org",
	"items": [{
		"subdomain": "portainer",
		"img": "portainer.png",
		"title": "Portainer",
		"description": "Gestor de contenedores Docker"
	},{
		"subdomain": "traefik",
		"img": "traefik.png",
		"title": "Traefik",
		"description": "Dashboard de Traefik, proxy inverso dinámico de contenedores y balanceador de carga"
	},{
		"subdomain": "nifi",
		"img": "nifi.png",
		"title": "Apache NiFi",
		"description": "Plataforma de procesamiento y distribución de datos"
	},{
		"subdomain": "vernemq",
		"img": "vernemq.png",
		"title": "VerneMQ",
		"description": "Dashboard de estado del broker MQTT"
	}]
};
